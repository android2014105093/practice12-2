package com.example.vrlab.myapplication;

import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MainActivity extends FragmentActivity {

    private GoogleMap googleMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }

    void init() {
        if (googleMap == null) {
            googleMap = ((SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.fragment1)).getMap();
            if (googleMap != null) {
                addMarker();
            }
        }
        googleMap.moveCamera(CameraUpdateFactory
                .newLatLngZoom(new LatLng(35.886869, 128.608408), 16));
    }

    private void addMarker() {
        googleMap.addMarker(new MarkerOptions().position(
                new LatLng(35.888664, 128.612122)).title("일청담").snippet("이일처엉다암"));
        googleMap.addMarker(new MarkerOptions().position(
                new LatLng(35.887984, 128.605098)).title("대운동장").snippet("대애우운도옹자앙"));
        googleMap.addMarker(new MarkerOptions().position(
            new LatLng(35.886133, 128.610106)).title("GS25").snippet("GGSS2255"));
    }
}